#include <stdio.h>

int main(void) {
  
  int edad = 21;

  if(edad > 21) {
    printf("You're over 21 years old!");
  }else if(edad == 21) {
    printf("You're 21 years old!");
  }else {
    printf("You're below 21 years old!");
  }

  printf("\nWhileLoop\n");

  int noy  = 0;

  while(noy < 5) {
    printf("You Are\n");
    noy++;
  }

  noy = 0;
  printf("\nDoWhileLoop\n");

  do {
    printf("Onii-chan!\n");
    noy++;
  }while(noy < 5);

  printf("\nForLoop\n");

  for(noy = 0; noy < 5; noy++) {
    printf("Onii-chan!\n");
  }

  return 0;
}