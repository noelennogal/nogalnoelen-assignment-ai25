<?php  
  $edad = 21;

  echo "<br>If-Elseif-Else<br>";
  if($edad > 21) {
    echo "You're over 21 years old!<br>";
  }elseif($edad == 21) {
    echo "You're 21 years old!<br>";
  }else {
    echo "You're below 21 years old!<br>";
  }

  echo "<br>WhileLoop<br>";
  $noy = 0;
  while($noy < 5) {
    echo "You are!<br>";
    $noy++;
  }

  echo "<br>Do-WhileLoop<br>";
  $noy = 0;
  do {
    echo "You Are!<br>";
    $noy++;
  }while($noy < 5);

  echo "<br>For Loop<br>";
  for($noy = 0; $noy < 5; $noy++) {
    echo "You Are!<br>";
  }

  echo "<br>ForEachLoop<br>";
  $numero = array(10, 34, 65, 130, 88);
  foreach($numero as $a) {
    echo $a . "<br>";
  }
?>