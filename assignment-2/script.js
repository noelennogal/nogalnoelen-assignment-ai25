var edad = 21;

if(edad > 21) {
  console.log("You're over 21 years old!");
}else if(edad = 21) {
  console.log("You're 21 years old!");
}else {
  console.log("You're below 21 years old!");
}

var noy = 0;
console.log("\nWhileLoop\n");

while(noy < 5) {
  console.log("You Are!\n");
  noy++;
}

noy = 0;
console.log("\nDoWhileLoop\n");

do {
  console.log("You Are!\n");
  noy++;
}while(noy < 5);

console.log("\nForLoop\n");

for(noy = 0; noy < 5; noy++) {
  console.log("You Are!\n");
}