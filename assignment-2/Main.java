class Main {
  public static void main(String[] args) {

    System.out.println("if,else if,else statement");
    int edad = 21;
    if(edad > 21) {
      System.out.println("You're over 21 years old.");
    }else if(edad == 21) {
      System.out.println("You're 21 years old.");
    }else {
      System.out.println("You're below 21 years old.");
    }
    System.out.println();

    System.out.println("WhileLoop");
    int noy = 0;
    while(noy < 5) {
      System.out.println("You Are!");
      noy++;
    }
    System.out.println();

    System.out.println("Do-While Loop");
    noy = 0;
    do {
      System.out.println("You Are!");
      noy++;
    }while(noy < 5);
    System.out.println();

    System.out.println("For Loop");
    for(noy = 0; noy < 5; noy++) {
      System.out.println("You Are!");
    }
    System.out.println();
    
    System.out.println("For-Each Loop");
    int numero[] = {10, 34, 65, 130, 88};
    for(int a : numero) {
      System.out.println(a);
}
}
}