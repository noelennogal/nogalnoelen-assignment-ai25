/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

    var myApp = new Vue({
        el: "#App",
        data: {
            info: "",
            infos: {'Name':'','Pnumber':''}
        },
        created: function(){
            this.Records();
        },
        methods: {
            Records: function() {
                axios.get('/display')
                .then(function (response) {
                    myApp.info = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
            },
            Add: function(){
                var input = this.infos;
                $this = this;
                if (input['Name'] != '' || input['Pnumber'] != ''){
                    axios.post('/store' , input)
                    .then(function(response){
                        myApp.Records();
                        $this.infos = {'Name':'','Pnumber':''};
                        alert('Saved!');
                    })
                    .catch(function(error){
                        console.log(error);
                    });
                } else {
                    alert("Please fill up all the fields");
                }
            },
            Delete: function(id){
                if(confirm('Are you sure?')){ 
                    axios.delete('/delete/'+id)
                    .then(function(response){
                    myApp.Records();
                    alert('Deleted!');
                    })
                    .catch(function(error){
                        console.log(error);
                    });
                }
            }
        }
    })
