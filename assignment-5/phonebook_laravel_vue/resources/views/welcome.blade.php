<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Phonebook Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
		<div id="app">
		<div class="row">
			<div class="col-md-4">
					<h1><center>Create New Contact</center></h1>
					<br>
					<div class="form-group">
					<b><input class="form-control" type = 'text' v-model = 'Name' placeholder = "Name"></b>
					</div>
					<div class="form-group">
					<b><input class ="form-control" type ='text' v-model = 'PhoneNumber'placeholder = "Phone Number"></b>
					</div>
					<button class="btn btn-success form-control" v-on:click = 'addPhoneInfo'>Add to Phonebook</button>
					<br>
					<br>
		    </div>
				 <div class="col-md-8">
					<h1><center>Contact List</center></h1>
					<table class="table table-striped">
					<tr>
					<h4><b>
					<th class="noCenter">Name</th>
                    <th class="noCenter">Phone Number</th>
                    <th colspan="6">Action</th>
               		</b></h4>
					</tr>	
						<tr v-for = "user in phone_info">
						<td>@{{ user.Name }}</td>
						<td>@{{ user.PhoneNumber }}</td>
						<td align="center">
						<td><button class="btn btn-success form-control" v-on:click = 'deletePhoneInfo(user.id)'>Delete</button></td>
						</td>
						</tr>
					</table>
				</div>
		</div>
	</div>
    </body>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
	<script>
 var app = new Vue({
 		el : '#app',
 		data : {
 			id : null,
 			Name: '', 
			PhoneNumber: '',
 			phone_info: '',

 		},
 		mounted: function () {
 			axios.get('/functionDisplay')
 			.then(function(response){
 				app.phone_info = response.data;
 				console.log(response);
 			})
 			.catch(function(err){
 				console.log(err);
 			});

 		},
 		methods: {
 			addPhoneInfo: function(){
 				if(this.Name != '' && this.PhoneNumber != ''){
				 	axios.post('/functionAdd', {
					Name: this.Name,
					PhoneNumber: this.PhoneNumber
					})
 					.then(function(response){
						alert(response.data);
 						window.location.reload();
 					})
 					.catch(function(err){
 						console.log(err);
 					});
 				}
			 	else alert('Missing input(s). Try again');
			 },
 			deletePhoneInfo: function(id){
 				axios.post('/functionDelete/'+id)
 				.then(function(response){
 					alert('Data deleted!');
 					window.location.reload();
 				})
 				.catch(function(err){
 					console.log(err);
 				})
 			}

 		}
 });
</script>
</html>
