<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTable extends Migration
{
    
    public function up()
    {
        Schema::create('phonebookusers', function (Blueprint $table){
			$table -> increments('id');
			$table -> string('Name');
            $table -> integer ('PhoneNumber');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
